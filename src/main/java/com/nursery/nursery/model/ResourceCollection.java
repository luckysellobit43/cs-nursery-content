package com.nursery.nursery.model;

import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class ResourceCollection<T> implements Serializable {
    private static final long serialVersionUID = 2512775015438722474L;

    static final ResourceCollection EMPTY_RESOURCE_COLLECTION = new ResourceCollection(0,0,0,0, Collections.emptyList());

    public static <T> ResourceCollection<T> emptyResourceCollection() {return EMPTY_RESOURCE_COLLECTION;}

    public static <T> boolean isEmpty(ResourceCollection<T> collection) {
        return collection == null || CollectionUtils.isEmpty(collection.getItems());
    }

    public ResourceCollection() {
    }

    public ResourceCollection(int currentPage, int pageCount, long totalPages, long totalElements,  List<T> items) {
        this.currentPage = currentPage;
        this.totalPages = totalPages;
        this.pageCount = pageCount;
        this.totalElements = totalElements;
        this.items = items;
    }

    private int currentPage;
    private long totalPages;
    private int pageCount;
    private long totalElements;
    private List<T> items;

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public long getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(long totalPages) {
        this.totalPages = totalPages;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }

    public List<T> getItems() {
        if (items == null) {
            items = new LinkedList<>();
        }
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(int totalElements) {
        this.totalElements = totalElements;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResourceCollection<?> that = (ResourceCollection<?>) o;
        return currentPage == that.currentPage &&
                totalPages == that.totalPages &&
                pageCount == that.pageCount &&
                totalElements == that.totalElements &&
                Objects.equals(items, that.items);
    }

    @Override
    public int hashCode() {

        return Objects.hash(currentPage, totalPages, pageCount, totalElements, items);
    }
}
