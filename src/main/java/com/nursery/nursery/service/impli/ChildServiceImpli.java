package com.nursery.nursery.service.impli;

import com.github.javafaker.Faker;
import com.google.common.collect.Lists;
import com.nursery.nursery.entity.Student;
import com.nursery.nursery.model.ResourceCollection;
import com.nursery.nursery.repository.ChildRepository;
import com.nursery.nursery.service.ChildService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.ResourceBundle;

@Service
public class ChildServiceImpli implements ChildService {

    private final ChildRepository childRepository;

    @Autowired
    public ChildServiceImpli(ChildRepository childRepository) {
        this.childRepository = childRepository;
    }

    @Override
    public ResourceCollection<Student> loadAllChildren(Integer page, Integer pageSize) throws Exception {
        Page<Student> pageableStudents = childRepository.findAll(PageRequest.of(page, pageSize));
        return new ResourceCollection(page, pageSize,  pageableStudents.getTotalPages(), pageableStudents.getTotalElements(), pageableStudents.getContent());
    }

    @Override
    public Student findChildByIdNumber(String idNumber) {
        return null;
    }

    @Override
    public Student saveChild(Student student) {

        while (true) {
            Faker faker = new Faker();
            Student student1 = new Student();
            student1.setFirstName(faker.name().firstName());
            student1.setFirstName(faker.name().firstName());
            student1.setIdNumber(faker.idNumber().valid());
            student1.setPassport(faker.idNumber().invalid());
            Student save = childRepository.save(student1);
            System.out.println(save.getId());
        }


    }
}
