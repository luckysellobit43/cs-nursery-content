package com.nursery.nursery.service;

import com.nursery.nursery.entity.Student;
import com.nursery.nursery.model.ResourceCollection;

public interface ChildService {

    public ResourceCollection<Student> loadAllChildren(Integer page, Integer pageSize) throws Exception;

    public Student findChildByIdNumber(String idNumber);

    public Student saveChild(Student student);

}
