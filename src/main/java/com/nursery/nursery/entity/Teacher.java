package com.nursery.nursery.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nursery.nursery.entity.abstraction.PersonBase;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Teacher extends PersonBase {


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "class_id")
    @JsonIgnore
    protected NursaryClass nursaryClass;

    public NursaryClass getNursaryClass() {
        return nursaryClass;
    }

    public void setNursaryClass(NursaryClass nursaryClass) {
        this.nursaryClass = nursaryClass;
    }
}
