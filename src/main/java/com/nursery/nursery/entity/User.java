package com.nursery.nursery.entity;

import com.nursery.nursery.entity.abstraction.BaseEntity;

import javax.persistence.Entity;

@Entity
public class User extends BaseEntity {

    public User() {
    }

    private String userName;
    private String passWrd;
    private String email;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWrd() {
        return passWrd;
    }

    public void setPassWrd(String passWrd) {
        this.passWrd = passWrd;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
