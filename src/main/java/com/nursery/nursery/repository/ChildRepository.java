package com.nursery.nursery.repository;

import com.nursery.nursery.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChildRepository extends JpaRepository<Student, Long> {

}
