package com.nursery.nursery.controller.impli;

import com.nursery.nursery.controller.ChildController;
import com.nursery.nursery.entity.Student;
import com.nursery.nursery.model.ResourceCollection;
import com.nursery.nursery.service.ChildService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("child")
public class ChildControllerImpli implements ChildController {

    private final ChildService childService;

    @Autowired
    public ChildControllerImpli(ChildService childService) {
        this.childService = childService;
    }

    @RequestMapping(path = "ping",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public String ping() {
        return "Student Service is reachable";
    }

    @RequestMapping(path = "listAllChildren",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public ResourceCollection<Student> listAllChildren(@RequestParam(value="page", defaultValue="0") Integer page,
                                                       @RequestParam(value="pageSize", defaultValue="5") Integer pageSize) throws Exception {
        return childService.loadAllChildren(page, pageSize);
    }

    @RequestMapping(path = "updateChild", produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    @Cacheable("listAllChildren")
    public Student updateChild() {
        return childService.saveChild(new Student());
    }
}
