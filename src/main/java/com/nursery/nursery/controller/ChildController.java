package com.nursery.nursery.controller;

import com.nursery.nursery.entity.Student;
import com.nursery.nursery.model.ResourceCollection;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.Serializable;
import java.util.List;

public interface ChildController extends Serializable {

    public ResourceCollection<Student> listAllChildren(@RequestParam(value="page", defaultValue="0") Integer page, @RequestParam(value="pageSize", defaultValue="5") Integer pageSize) throws Exception;

    public Student updateChild();

}
